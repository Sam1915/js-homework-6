function createNewUser() {
    let firstName = prompt('Select your NAME:');
    let lastName = prompt('Select your LAST NAME:');
    let birthday = prompt('When it`s your birthday: ');

    const newUser = {
        getLogin: function () {
            return newUser.firstName.toLowerCase()[0] + newUser.lastName.slice().toLowerCase();
        },
        getAge: function () {
            const birthdayDate = birthday.split('.');
            const currentDate = new Date();
            const birthDate = new Date(+birthdayDate[2], birthdayDate[1] - 1, +birthdayDate[0]);
            const diff = currentDate - birthDate;
            return Math.floor(diff / (1000 * 60 * 60 * 24 * 365));
        }
    };

    if (firstName) {
        newUser.firstName = firstName;
    }
    if (lastName) {
        newUser.lastName = lastName;
    }
    return newUser;
}
let res = createNewUser();
console.log(res);
console.log(res.getLogin());
console.log(res.getAge());